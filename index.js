/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/


	
	//first function here:

function welcomeMesagge () {

		let firstName = prompt ("Enter your First Name: ");
		let lastName = prompt ("Enter your Last Name: ");
		let age = prompt ("Enter your Age: ");
		let address = prompt ("Enter your Address: ");

		console.log ("Hello, " + firstName +" "+ lastName );
		console.log ("You are " + age + " years old." );
		console.log ("You live in " + address );
}		

welcomeMesagge();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function faveBands () {

	console.log ("1. The Beatles");
	console.log ("2. Metallica");
	console.log ("3. The Eagles");
	console.log ("4. L'arc~en~Ciel");
	console.log ("5. Eraserheads");
}

faveBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function faveMovies() {
	let movie1 = "1. The Godfather";
	let rating1 = "97%";
	console.log (movie1);
	console.log ("Rotten Tomatoes Rating: " + rating1);

	let movie2 = "2. The Godfather, Part II";
	let rating2 = "96%";
	console.log (movie2);
	console.log ("Rotten Tomatoes Rating: " + rating2);

	let movie3 = "3. Shawshank Redemption";
	let rating3 = "91%";
	console.log (movie3);
	console.log ("Rotten Tomatoes Rating: " + rating3);

	let movie4 = "4. To Kill A Mockingbird";
	let rating4 = "93%";
	console.log (movie4);
	console.log ("Rotten Tomatoes Rating: " + rating4);

	let movie5 = "5. Psycho";
	let rating5 = "96%";
	console.log (movie5);
	console.log ("Rotten Tomatoes Rating: " + rating5);
}

faveMovies();
// console.log ();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// function printUsers(){

// }
// let printFriends =
 function printUsers (){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

// console.log (printFriends);
printUsers();

